<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('aimeos_header')
    <title>Aimeos on Laravel</title>

    <link type="text/css" rel="stylesheet" href='https://fonts.googleapis.com/css?family=Roboto:400,300'>
    {{--<link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    {{--Including aimeos main css file in base.blade.php--}}
    @yield('aimeos_styles')

    {{--Adding my Custom CSS styling--}}
    <link type="text/css" rel="stylesheet" href="{{asset('css/shalmi.css')}}">

</head>
<body>
{{--<nav class="navbar navbar-default navbar-fixed-top">--}}
    <nav class="navbar navbar-default fixed-top bg-dark navbar-dark navbar-expand-sm">

    <div class="container-fluid">
        <!-- hamburger setup icon in left part of header for small screen -->
        <div class="navbar-header">
            <!-- Boottrap 3 implementation -->
            {{--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">--}}
                {{--<span class="sr-only">Toggle Navigation</span>--}}
                {{--<span class="icon-bar"></span>--}}
                {{--<span class="icon-bar"></span>--}}
                {{--<span class="icon-bar"></span>--}}
            {{--</button>--}}

            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="navbar-toggler-icon"></span>
            </button>

            <a class="navbar-brand" href="#">ShalmiOnline</a>
        </div>

        <!-- Navigation bar -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class=" nav navbar-nav ">
                <li class="nav-item"><a class="nav-link" href="{{route('login')}}">Login</a></li>
                {{--<li><a>or</a></li>--}}
                <li class="nav-item"><a class="nav-link" href="{{route('register')}}">Register</a></li>
                <li class="nav-item"><a class="nav-link" href="/">New Arrivals</a></li>
            </ul>
<!-- Catalog filter and search component see list.blade file -->
            {{--<div class="nav navbar-nav navbar-right">--}}
            <div class="nav navbar-nav ml-auto">
                @yield('aimeos_head')
            </div>
        </div>
    </div>
</nav>




<div class="col-sm-12">
    @yield('aimeos_nav')
    <!--Bootstrap4 Carousel -->
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100 img-thumbnail" src="{{asset('images/cofee_clutch_720_320.jpg')}}" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">
                        <h4>Genuine Leather stuff</h4><br>
                        <p>Lorem ipsum Lorem ipsumLorembr <br>
                            ipsumLorem ipsum</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 img-thumbnail" src="{{asset('images/bowtie_contrast.jpg')}}" alt="Second slide">
                    <div class="carousel-caption d-md-block">
                        <h4>Lorem Ipsum</h4><br>
                        <p>Lorem ipsum Lorem ipsumLorembr <br>
                            ipsumLorem ipsum</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 img-thumbnail" src="{{asset('images/formal1.jpg')}}" alt="Third slide">
                    <div class="carousel-caption d-md-block">
                        <h4>Lorem Ipsum</h4><br>
                        <p>Lorem ipsum Lorem ipsumLorembr <br>
                            ipsumLorem ipsum</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 img-thumbnail" src="{{asset('images/iphonecover.jpg')}}" alt="Fourth slide">
                    <div class="carousel-caption d-md-block">
                        <h4>Lorem Ipsum</h4><br>
                        <p>Lorem ipsum Lorem ipsumLorembr <br>
                            ipsumLorem ipsum</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 img-thumbnail" src="{{asset('images/khadi1_320.jpg')}}" alt="Fifth slide">
                    <div class="carousel-caption d-md-block">
                        <h4>Lorem Ipsum</h4><br>
                        <p>Lorem ipsum</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 img-thumbnail" src="{{asset('images/ladies_koti.jpg')}}" alt="sixth slide">
                    <div class="carousel-caption d-md-block">
                        <h4>Lorem Ipsum</h4><br>
                        <p>Lorem ipsum</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 img-thumbnail" src="{{asset('images/khaddar_black.jpg')}}" alt="seventh slide">
                    <div class="carousel-caption d-md-block">
                        <h4>Lorem Ipsum</h4><br>
                        <p>Lorem ipsum</p>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

    {{--@yield('aimeos_stage')--}}
    @yield('aimeos_body')
    @yield('aimeos_aside')
    @yield('content')
</div>

<!-- Scripts -->

{{--Bootstrap 3--}}
<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
{{--<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--}}

{{--Bootstrap 4--}}
{{--this version of jquery not working out of box needs investigation--}}
{{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


@yield('aimeos_scripts')
{{--My own customized JS file--}}
<script src="{{asset('js/shalmi.js')}}"></script>

</body>
</html>