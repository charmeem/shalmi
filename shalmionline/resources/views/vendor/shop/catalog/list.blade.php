
{{--Custom view file adapted for shalmi--}}
{{--This view is rendered via controller mentioned in php artisan list:route--}}


@extends('shop::base')
 {{--this is base.blade.php file--}}


{{--Adding my Custom CSS styling--}}
{{--doesnot work so i added it in app.blade.php--}}
{{--@section('aimeos_styles')--}}
    {{--<link type="text/css" rel="stylesheet" href="{{asset('css/aimeos.css')}}">--}}
{{--@stop--}}


@section('aimeos_header')
    <?= $aiheader['basket/mini'] ?>
    <?= $aiheader['catalog/filter'] ?>
    <?= $aiheader['catalog/stage'] ?>
    <?= $aiheader['catalog/lists'] ?>
@stop

@section('aimeos_head')
    <?= $aibody['basket/mini'] ?>
@stop

<!-- points to catalog/body-standard.php
     include Search input  and Tree ( catagory) feature
-->
@section('aimeos_nav')
    <?= $aibody['catalog/filter'] ?>
@stop
<!-- -->

@section('aimeos_stage')
    <?= $aibody['catalog/stage'] ?>
@stop

@section('aimeos_body')
    <?= $aibody['catalog/lists'] ?>
@stop