<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return redirect('list');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/** Displaying Users for Testing Purposes ONLY
 *  1. Create resource routes and a custom controller
 *  2. check with artisan route:list command
 */
//Route::resource('admin/users', 'AdminUsersController');

//Route::group(config('shop.routes.default', ['middleware' => ['web']]), function() {
//    Route::match( array( 'GET', 'POST' ), 'list', array(
//        'as' => 'aimeos_shop_list',
//        'uses' => 'Aimeos\Shop\Controller\CatalogController@listAction'
//    ));
//});
